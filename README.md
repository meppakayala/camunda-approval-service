# Camunda Workflow and Decision Automation

This Code can be leveraged to demonstrate Workflow and Decision Automation using Camunda.

Camunda Workflow Engine is embedded in Spring Boot Application.

![alt text](Architecture.png "Architecture")

# Instructions to Setup and Run this Repository.

## Instructions to Run Locally from IDE(Spring Tool Suite) on your laptop or dev environment:

Import this repository as Maven project onto your Favourite IDE like Spring Tool Suite and run each app as Spring Boot App.

    Camunda Workflow Welcome : http://localhost:8080/
    
    User : demo
    
    Pass : demo
    
    Camunda Cockpit : http://localhost:8080/app/cockpit/default/
    
    BPMN : https://gitlab.com/meppakayala/camunda-approval-service/-/blob/master/src/main/resources/fda-drug-approval.bpmn
    
    DMN: https://gitlab.com/meppakayala/camunda-approval-service/-/blob/master/src/main/resources/fda-drug-evaluation.dmn

Update mail server details in mail-config properties

    Change "TO" email address to receive notifications in <camunda:inputParameter name="to">eppmur@gmail.com</camunda:inputParameter>

Start the process as below from any REST client
    
POST http://localhost:8080/rest/engine/default/process-definition/key/fda-drug-approval/start
